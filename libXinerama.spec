%bcond_without static

Name:          libXinerama
Version:       1.1.5
Release:       1
Summary:       Xinerama extension to the X11 Protocol
License:       MIT
URL:           https://xorg.freedesktop.org/
Source0:       https://xorg.freedesktop.org/releases/individual/lib/%{name}-%{version}.tar.gz

BuildRequires: xorg-x11-util-macros autoconf automake libtool pkgconfig
BuildRequires: xorg-x11-proto-devel libX11-devel libXext-devel
Requires:      libX11

%description
Xinerama is an extension to the X Window System which enables
multi-headed X applications and window managers to use two or more
physical displays as one large virtual display.

%package       devel
Summary:       Development files and Header files for %{name}
Requires:      %{name} = %{version}-%{release} 

%description   devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -ivf
%configure  \
%if %{with static}
    --disable-static
%endif
%make_build

%install
%make_install

%ldconfig_scriptlets

%files
%defattr(-,root,root)
%license COPYING
%{_libdir}/libXinerama.so.*

%files  devel
%defattr(-,root,root)
%{_includedir}/X11/extensions/*.h
%{_libdir}/pkgconfig/xinerama.pc
%if %{without static}
%{_libdir}/*.a
%endif
%{_libdir}/libXinerama.so
%exclude %{_libdir}/*.la

%files  help
%defattr(-,root,root)
%{_mandir}/man3/*

%changelog
* Fri Feb 03 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 1.1.5-1
- update to 1.1.5

* Sat Oct 22 2022 wangkerong <wangkerong@h-partners.com> - 1.1.4-6
- disable static library by default

* Sat Oct 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.1.4-5
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:change the directory of the license files

* Mon Sep 9 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.1.4-4
- Type: enhancement
- ID:   NA
- SUG:  NA
- DESC: rebuilt spec

* Wed Aug 28 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.1.4-3
- Package init


